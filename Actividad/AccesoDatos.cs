﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Actividad
{
    class AccesoDatos
    {
        // Aca se agrupan toda la funcionalidad que tiene que ver con la base de datos en si

        private SqlConnection conexion;

        //Constructor de la clase
        public AccesoDatos()
        {
            conexion = new SqlConnection();

            conexion.ConnectionString = @"Data source=.\sqlexpress; Initial Catalog=AmbAutos; Integrated Security= SSPI";
        }


        private void Abrir()
        {
            conexion.Open();
        }

        private void Cerrar()
        {
            conexion.Close();
        }


        public List<Comparacion> ListarInformacion()
        {
            List<Comparacion> informacion = new List<Comparacion>();

            List<Colorcito> colores = ListarColores();

            Abrir();

            string sql = "select * from primertabla";
            SqlCommand comandito = CrearComando(sql);
            SqlDataReader lector = comandito.ExecuteReader();

            

            while (lector.Read())
            {
                Comparacion informacionnueva = new Comparacion();

                informacionnueva.Auto = lector["AUTO"].ToString();
                informacionnueva.Patente = lector["PATENTE"].ToString();
                informacionnueva.Marca = lector["MARCA"].ToString();
                int indice = 0;

                while (informacionnueva.Color== null)
                {
                    
                    if (lector.GetInt32(1) == colores[indice].ID)
                    {
                        informacionnueva.Color = colores[indice];
                    }
                    else
                    {
                        indice++;
                    }

                }


                informacion.Add(informacionnueva);
            }

            lector.Close();
            Cerrar();

            return informacion;

        }

        public int Escribir(string sql)

        {

            int filasAfectadas = 0;

            Abrir();

            SqlCommand comando = new SqlCommand();

            comando.Connection = conexion;

            comando.CommandType = CommandType.Text;

            comando.CommandText = sql; // Recibe de arriba lo que hay que escribir


            try
            {
                filasAfectadas = comando.ExecuteNonQuery(); // Ejecuta el comando
            }

            catch (SqlException ex) // Para que sirve ex ?

            {
                filasAfectadas = -1;
            }



            Cerrar();

            return filasAfectadas;  // Dependiendo de lo que vayamos a hacer, si tantas filas afectadas se hizo bien o no

        }


        private SqlCommand CrearComando(string sql)
        {
            SqlCommand comandito = new SqlCommand();

            comandito.Connection = conexion;

            comandito.CommandType = CommandType.Text;

            comandito.CommandText = sql;

            return comandito;
        }


        // ---------------------- parte del problema --------------------//
        public List<Colorcito> ListarColores()
        {

            List<Colorcito> colores = new List<Colorcito>();

            Abrir();

            string sql = "Select * from color";

            SqlCommand comandito = CrearComando(sql);

            SqlDataReader lector = comandito.ExecuteReader();

            while (lector.Read())
            {
                Colorcito color = new Colorcito();
                color.ID = int.Parse(lector["id_color"].ToString());
                color.Color = lector["Color"].ToString();

                colores.Add(color);
            }

            lector.Close();

            Cerrar();

            return colores;



        }

    }
}
