﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Actividad
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

       
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            EnlazarColores();
            EnlazarInformacion();
        }


        // ------------ BOTON INSERTAR  ------------- //


        private void button2_Click(object sender, EventArgs e)
        {
            Colorcito color = (Colorcito)comboBox1.SelectedItem;

            string consulta;

            consulta = "Insert into primertabla (auto, id_color, patente, marca) values ('" + textBox1.Text + "', " + color.ID.ToString() + ", '" + textBox3.Text + "', '" + textBox4.Text + "')";


            AccesoDatos acceso = new AccesoDatos();

            int resultado = acceso.Escribir(consulta);

            if(resultado == -1)
            {
                MessageBox.Show("Hubo un error", "ERROR", MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Ok");
            }

            EnlazarInformacion();

            GC.Collect();

        }

        // ------------ BOTON BORRAR ------------- // FUNCIONA
        private void button3_Click(object sender, EventArgs e)
        {

            AccesoDatos acceso = new AccesoDatos();

            foreach ( DataGridViewRow registro in dataGridView1.SelectedRows) // Esto va ir iterando todos los elementos seleccionados de la lista
            {

                Comparacion informacionnueva = (Comparacion)registro.DataBoundItem; //Voy a retirar el objeto que esta enlazado

                string sql = "DELETE FROM PRIMERTABLA WHERE PATENTE='" + informacionnueva.Patente.ToString()+"'";

                acceso.Escribir(sql);
            }

            EnlazarInformacion();
            acceso = null;

            GC.Collect();
        }

        void EnlazarColores()
        {
            AccesoDatos acceso = new AccesoDatos();

            comboBox1.DataSource = null; //Descenlazamos 
            comboBox1.DataSource = acceso.ListarColores();

        }

        void EnlazarInformacion()
        {
            AccesoDatos acceso = new AccesoDatos();
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = acceso.ListarInformacion();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }


        // ------------ BOTON EDITAR ------------- //
        private void button4_Click(object sender, EventArgs e)
        {
            string sql = "UPDATE PRIMERTABLA SET AUTO = '" + textBox1.Text; 
            sql += "', COLOR='" + textBox2.Text + "', PATENTE='" + textBox3.Text + "', MARCA='" + textBox4.Text + "' WHERE ID=" + lblCodigo.Text;

            AccesoDatos acceso = new AccesoDatos();

            acceso.Escribir(sql);

            acceso = null;

            GC.Collect();


        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }



        private void button5_Click(object sender, EventArgs e)
        {
            AccesoDatos acceso = new AccesoDatos();

            string sql = "insert into color (color) values ('" + textBox2.Text + "')";

            if (acceso.Escribir(sql) == -1)
            {
                MessageBox.Show("Hubo un error", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            

            EnlazarColores();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
