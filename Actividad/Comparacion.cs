﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Actividad
{
    class Comparacion
    {

        // -------- todo ok ---------- //

        private int codigo; // El id de los registros 

        public int Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        private string auto;

        public  string Auto
        {
            get { return auto; }
            set { auto = value; }
        }

        private Colorcito color;  

        public Colorcito Color
        {
            get { return color; }
            set { color = value; }
        }

        private string patente;

        public string Patente
        {
            get { return patente; }
            set { patente = value; }
        }

        private string marca;

        public string Marca
        {
            get { return marca; }
            set { marca = value; }
        }


    }
}
